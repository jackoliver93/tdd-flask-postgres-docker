# src/tests/test_users.py

# tests for our RESTful routes for getting all users, a single user and for adding a user

import json

from src.api.models import User

# POST ROUTE


def test_add_user(test_app, test_database):
    client = test_app.test_client()  # get the test app

    # call the post method on the /users endpoint
    resp = client.post(
        "/users",
        data=json.dumps({"username": "Oliver", "email": "ollie_batey@email.com"}),
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "ollie_batey@email.com was added!" in data["message"]


def test_add_user_invalid_json(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps({}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_invalid_json_keys(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps({"email": "john@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_duplicate_email(test_app, test_database):
    client = test_app.test_client()
    client.post(
        "/users",
        data=json.dumps({"username": "Oliver", "email": "ollie_batey@email.com"}),
        content_type="application/json",
    )
    resp = client.post(
        "/users",
        data=json.dumps({"username": "Oliver", "email": "ollie_batey@email.com"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Sorry. That email already exists." in data["message"]


# GET Single User Route


def test_single_user(test_app, test_database, add_user):

    # refactor since we have the factory as fixture pattern in conftest.py
    user = add_user("test_user", email="test_user@email.com")

    # request the new user's data from the database
    client = test_app.test_client()
    resp = client.get(f"/users/{user.id}")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert "test_user" in data["username"]
    assert "test_user@email.com" in data["email"]


def test_single_user_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/users/999")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 999 does not exist" in data["message"]


# GET All Users Route


def test_all_users(test_app, test_database, add_user):
    # first clear all users, that may be added from previous tests
    test_database.session.query(User).delete()

    # use add_user fixture to add two users to the database
    add_user("test_user_1", "test_user_1@email.com")
    add_user("test_user_2", "test_user_2@email.com")

    # query database
    client = test_app.test_client()
    resp = client.get("/users")
    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert len(data) == 2  # make sure there are indeed 2 users
    assert "test_user_1" in data[0]["username"]
    assert "test_user_1@email.com" in data[0]["email"]
    assert "test_user_2" in data[1]["username"]
    assert "test_user_2@email.com" in data[1]["email"]
