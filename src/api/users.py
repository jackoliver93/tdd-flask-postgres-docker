# src/api/users.py

from flask import Blueprint, request
from flask_restx import Api, Resource, fields

# import our database and our blueprint for a user table
from src import db
from src.api.models import User

users_blueprint = Blueprint("users", __name__)
api = Api(users_blueprint)


"""here we are registering our database model User with the API by
including it in its namespace. The dictionary of fields specifies
the shape of a payload, and we validate the input using fields,
which are similar to functionality provided by WTForms.
We say the input is filtered using the fields."""
user = api.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class UsersList(Resource):

    # tell the post method to expect a payload in the form defined by user and validate input
    @api.expect(user, validate=True)
    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = User.query.filter_by(email=email).first()
        if user:
            response_object["message"] = "Sorry. That email already exists."
            return response_object, 400

        db.session.add(User(username=username, email=email))
        db.session.commit()

        response_object["message"] = f"{email} was added!"
        return response_object, 201

    # GET all users goes here
    @api.marshal_with(user, as_list=True)
    def get(self):
        return User.query.all(), 200


# Add the user resource to our users file
class Users(Resource):
    @api.marshal_with(user)
    def get(self, user_id):
        user = User.query.filter_by(id=user_id).first()
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        return user, 200


api.add_resource(UsersList, "/users")

# now we add the User resource to the API, in the same way we added UserList
api.add_resource(Users, "/users/<int:user_id>")
