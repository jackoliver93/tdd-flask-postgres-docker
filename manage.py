import sys 

from flask.cli import FlaskGroup

from src import create_app, db
from src.api.models import User

app = create_app()#use factory pattern to get the app 
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()

@cli.command('seed_db')
def seed_db():
    db.session.add(User(username='Oliver', email="oliver@gmail.com"))
    db.session.add(User(username='Oliver Batey', email="oliver_batey@gmail.com"))
    db.session.commit()

if __name__ == '__main__':
    cli()